# University of Arizona Kuali Base Rhubarb Docker Container
---

### Description
This project defines an image with code dependencies (e.g. Ruby) and service accounts (non-prod or prod) for a Rhubarb Docker container.

It is based on an image managed by MET (Burgundy Group). The images are stored in another AWS account that we have been granted access to for Docker image build purposes.

It originally combined the code in the docker-ksd-security and docker-ksd-accounts projects, to reduce complication. It was created for https://uarizona.atlassian.net/browse/FIN-155 in order to simplify the Rhubarb Docker image. It was then updated for https://uarizona.atlassian.net/browse/FIN-1627 to change the underlying OS to Amazon Linux 2 inside the Rhubarb Docker container on a UAccess Financials application environment. It was then updated to switch to Amazon Linux 2023 for https://uarizona.atlassian.net/browse/FIN-3597.

### Building

#### Local
You will need a copy of the latest base image in order to do a local build. Suggested steps:
1. Run the two "Docker Baseline" jobs that will query the MET images and then copy an envy-base image to our KFS non-prod AWS account.
2. Use the "AWS Temp Credentials" process to establish connectivity to our KFS non-prod account from your local machine.
3. Run this command to log into ECR: `aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin 397167497055.dkr.ecr.us-west-2.amazonaws.com`
4. Build an image with a command such as this: `docker build -t kuali/rhubarb:rhubarb-base-nonprod --build-arg DOCKER_REPOSITORY=397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/base --build-arg BASE_DOCKER_TAG_NAME=amazonlinux-2023-2024.Sep.25 --build-arg TARGET_ENV=nonprod --force-rm .`
* If using a Mac, you may need to add `--platform=linux/amd64` to account for the processor; that is, it is not arm64.
* The BASE_DOCKER_TAG_NAME should match the one you grabbed in step #1.

This command will build an image for a non-prod environment and tag it with the name rhubarb-base-nonprod.

The purpose of the `TARGET_ENV` argument is to ensure the correct public SSH keys are included in the Docker image for the target deployment environment. Valid values for the `TARGET_ENV` are:

* nonprod
* prd

The default value is nonprod if one is not provided.

**Note for local testing:** also uncomment the lines related to `/var/log/auth.log` in the start-security-apps.sh script if you plan to run a Docker container using just this code, so that the container does not immediately exit.

#### Jenkins
Docker images are created in our UAF Jenkins instance in the "Rhubarb For KFS 7" tab of jobs.

The build command we effectively use is: `docker build -t kuali/rhubarb:rhubarb-base-<TARGET_ENV>-<DATE> --build-arg TARGET_ENV=$TARGET_ENVIRONMENT --force-rm .`

TARGET_ENVIRONMENT is nonprod or prd.

We then tag and push the image to AWS (Examples: `397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb:rhubarb-base-nonprod-2024-05-14` and `397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb:rhubarb-base-prd-2024-05-14`).

### Running
A sample run command (used for testing) is: `docker run -d --name=rhubarb-base --privileged=true kuali/rhubarb:rhubarb-base-nonprod`

This command will run a container called rhubarb-base based on the kuali/rhubarb:rhubarb-base-nonprod image. It needs to be run in privileged mode.

If using a Mac, you may need to add `--platform=linux/amd64` to account for the processor (i.e. it is not arm64).

NOTE: You will need to uncomment out the last line of the start-security-apps.sh script (`tail -f /var/log/auth.log`) if you are doing local testing. Otherwise the container will immediately exit.

### Technical Details

#### Ruby
When we switched to Amazon Linux 2023, we also made the decision to use the included version of Ruby that AWS supports. Currently this is version 3.2. See https://docs.aws.amazon.com/linux/al2023/release-notes/support-info-by-package.html#support-info-by-package-eol_ruby3.2.

##### SSH Keys
We are storing the public SSH keys for the kbatch and pssys users in this repository. The corresponding private keys are stored on the Control-M servers. We only have two sets of SSH keys; all non-production environments will use the same keypair because there is only one test instance of Control-M.

For any local testing with the kbatch or psssys user: the public key for each is in ssh/dev and they are identical. The private key is in stache in the "KFS Team Stuff" folder in the "kbatch Private SSH Key For Rhubarb Docker Container" entry.

If the SSH keys change, this project will need to be updated and a new Docker image will need to be created. We will also need to coordinate using the new private key(s) with the Control-M team.

We encountered errors (see FIN-3783) with changes made to move to Amazon Linux 2023 for FIN-3597. We had to re-enable ssh-rsa (https://docs.aws.amazon.com/linux/al2023/ug/ssh-host-keys-disabled.html) in order to continue to use current Control-M agents. When we move to version 21 agents, we will update the SSH keys used for kbatch and pssys. We will want to remove the workaround at that time.

##### Certs
The current default certificates expired on May 30, 2020. Following instructions on https://bundler.io/v2.0/guides/rubygems_tls_ssl_troubleshooting_guide.html, we ended up making changes to manually copy in a certificate for Ruby to use. This new certificate expires Dec 31 23:59:59 2028 GMT. See https://uarizona.atlassian.net/browse/FIN-1643 for more information.

As of FIN-3597, the instructions for the certificate are commented out. Kept the certificate for now, in case we need it in the future.