# For a container with common security applications, plus Kuali groups and related users

# Build from MET Amazon Linux 2023 image. Example tag: 204637156573.dkr.ecr.us-west-2.amazonaws.com/tbginc/envy-base:amazonlinux-2023-2024.Sep.25
ARG DOCKER_REPOSITORY
ARG BASE_DOCKER_TAG_NAME
FROM $DOCKER_REPOSITORY:$BASE_DOCKER_TAG_NAME
LABEL maintainer="U of A Kuali DevOps <katt-support@list.arizona.edu>"

# Build variables
# Default is set to "dev"
ARG TARGET_ENV=dev

# Copy in start script and make executable
COPY bin /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Install version of Ruby and ruby-devel (needed for bundle install) provided by AWS and set GEM_HOME (based on 'gem environment' command during local development)
RUN yum install -y ruby ruby3.2-devel
ENV GEM_HOME="/usr/share/ruby3.2-gems"

# Setting up gem stuff, like in https://github.com/docker-library/ruby/blob/master/Dockerfile.template
ENV BUNDLE_SILENCE_ROOT_WARNING=1 \
	BUNDLE_APP_CONFIG="$GEM_HOME"
ENV PATH=$GEM_HOME/bin:$PATH
RUN set -eux; \
# adjust permissions of GEM_HOME for running "gem install" as an arbitrary user
chmod 1777 "$GEM_HOME"

# Installs the right headers for gcc to perform the actual check for gem installs. Fixes bundle install errors in rhubarb. Scott found the fix.
RUN yum install -y clang-devel

# Install additional utilities
RUN yum install -y procps cronie

# FIN-1643 Copying in new certificate directly. Commented out for FIN-3597 and move to AL2023 and Ruby 3.2.2
#COPY configurations/certs/AddTrustExternalCARoot.pem /usr/local/rvm/rubies/ruby-1.9.3-p551/lib/ruby/gems/1.9.1/gems/bundler-1.7.6/lib/bundler/ssl_certs/
#COPY configurations/certs/AddTrustExternalCARoot.pem /usr/local/rvm/rubies/ruby-1.9.3-p551/lib/ruby/site_ruby/1.9.1/rubygems/ssl_certs/

# Create kuali, kfs-services, kfs-writers, and kfs-writers-sup groups with specific gids
# This matches the WRITERS_GROUP configuration variable in docker-kfs directory setup scripts
# 1002 for the non prods in the nonprd account, 1003 for SUP.
RUN groupadd -r -g 1004 kuali && \
    groupadd -r -g 1001 kfs-services && \
    groupadd -r -g 1002 kfs-writers && \
    groupadd -r -g 1003 kfs-writers-sup

# Create kbatch user with specific uid, put in groups, and set default shell
# Original UID of 998 conflicts with RHEL8/RHEL9/AL2023 system accounts.
RUN useradd -s /bin/bash -u 1998 -g 1004 kbatch && \
    usermod -a -G kuali,kfs-services,kfs-writers,kfs-writers-sup kbatch

# Create pssys user with specific uid, put in groups, and set default shell
# Original UID of 997 conflicts with RHEL8/RHEL9/AL2023 system accounts.
RUN useradd -s /bin/bash -u 1997 -g 1004 pssys && \
    usermod -a -G kuali,kfs-services,kfs-writers,kfs-writers-sup pssys

# Set up .ssh directories and copy in public SSH keys for each user
RUN mkdir -p /home/kbatch/.ssh
WORKDIR /home/kbatch/.ssh
COPY ssh/$TARGET_ENV/kbatch.pub .
RUN cat kbatch.pub > authorized_keys

RUN mkdir -p /home/pssys/.ssh
WORKDIR /home/pssys/.ssh
COPY ssh/$TARGET_ENV/pssys.pub .
RUN cat pssys.pub > authorized_keys

# Update user home directory permissions
RUN chown -R kbatch:kuali /home/kbatch/
RUN chmod 700 /home/kbatch/.ssh
RUN chown -R pssys:kuali /home/pssys/
RUN chmod 700 /home/pssys/.ssh

# enable legacy system crypto policy for Control-M v19 agents https://docs.aws.amazon.com/linux/al2023/ug/ssh-host-keys-disabled.html
RUN yum install -y crypto-policies-scripts
RUN update-crypto-policies --set LEGACY

ENTRYPOINT ["/usr/local/bin/start-security-apps.sh"]